import {Schema, model} from 'mongoose';


const userSchema = new Schema({
	nom: {
		type:String,
		required: true
		},
	prenom:{
		type:String,
		required: true
		},
	password: {
		type: String,
		required: true
	  },
	 email: {
		type: String,
		required: true,
		trim: true,
		unique: true,
	  },
	  typeU: {
		type: Schema.Types.ObjectId,
		ref: 'TypeUser',
		required: true
	}
	
	
	
});

const User = model('User', userSchema);

export default User; 