import {Schema, model} from 'mongoose';


const CitationSchema = new Schema({
	title: {
		type:String,
		required: true
		},
	content:{
		type:String,
		required: true
		},
	image:{
		type:String,
		required: true
	},
		
	user_id:{
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	
	
	
	
});

const Citation = model('Citation', CitationSchema);

export default Citation; 