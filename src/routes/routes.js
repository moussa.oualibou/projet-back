import {Router} from 'express';
import UserController from '../controllers/users.controller';
import CitationController from '../controllers/citation.controller';
import TypeUserController from '../controllers/typeUser.controller';

const router = Router();

router.get('/hello', function(req,res){
	console.log('Hello');
});

//Users
router.get('/users', UserController.list);
router.post('/users', UserController.create);
router.get('/users/:id', UserController.details);
router.delete('/users/:id', UserController.delete);
router.put('/users/:id', UserController.update);

//Citation
router.get('/citations', CitationController.list);
router.post('/citations', CitationController.create);
router.get('/citations/:id', CitationController.details);
router.delete('/citations/:id', CitationController.delete);
router.put('/citations/:id', CitationController.update);

//TypeUser
router.get('/typeuser/:id', TypeUserController.details);
router.post('/typeuser', TypeUserController.create);
export default router;