import TypeUser from "../models/TypeUser";
class TypeUserController{
	

	
	static async create(req, res){
		let status = 200;
		let body = {};
	
		try{
			let typeUser= await TypeUser.create({
			status: req.body.status
			});
			body= {typeUser, 'message':'typeUser created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}

	static async details(req, res){
		let status = 200;
		let body = {};
	
		try{
			//dans un find on peu mettre une condition 
			let typeUser= await TypeUser.findById(req.params.id).populate('user_id');
			body= {typeUser, 'message': 'citation Details '};
			
			
		} catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}
}
export default TypeUserController;