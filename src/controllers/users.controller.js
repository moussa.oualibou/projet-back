import User from "../models/User";
class UserController{
	static async list(request, response){
		let status = 200;
		let body = {};
		
		/**
		*Plusieurs méthodes :
		Post.find() lister tous les posts //VOIR DOC MONGOOSE
		post.findOne(){slug: "monmon-titre"});
		Post.findById("6868");
		*/
		try{
			let users= await User.find().populate('typeU');
			body= {users, 'message': 'List users'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}
	
	static async create(request, response){
		let status = 200;
		let body = {};
	
		try{
			let user= await User.create({
			nom: request.body.nom,
			prenom : request.body.prenom,
			password: request.body.password,
			email : request.body.email,
			typeU: request.body.typeU
			});
			body= {user, 'message':'user created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}
	
	static async details(request, response){
		let status = 200;
		let body = {};
	
		try{
			let id = request.params.id;
			let user= await User.findById(id);
			body= {user, 'message': 'Details '};
			
			
		} catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}

/*
	*Delete
	*/
	static async delete(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			await User.deleteOne(id);
			body = {user, 'message': 'Details'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}

	/*
	*update user
	*/
	static async update(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			let user= await user.findById(id);
			await user.update(request.body)
			body = {user, 'message': 'Details'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}
}
export default UserController;