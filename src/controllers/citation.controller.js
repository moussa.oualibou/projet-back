import Citation from "../models/Citation";
class CitationController{
	

	static async create(req, res){
		let status = 200;
		let body = {};
	
		try{
			let citation= await Citation.create({
			title: req.body.title,
			content : req.body.content,
			image : req.body.image,
			user_id : "5de823b74aae340e651335f1"
			});
			body= {citation, 'message':'citation created'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}
	static async list(request, response){
		let status = 200;
		let body = {};
		
		try{
			let citations= await Citation.find().populate('user_id');
			body= {citations, 'message': 'List Citations'};
		
		}catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}
	
	
	static async details(request, response){
		let status = 200;
		let body = {};
	
		try{
			let id = request.params.id;
			let citations= await Citation.findById(id);
			body= {citations, 'message': 'Details '};
			
			
		} catch(error){
			status= 500;
			body = {'message': error.message};
		}
		
		return response.status(status).json(body);
		
		
	}

	/*
	*Delete
	*/
	static async delete(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			await Citation.deleteOne(id);
			body = {citation, 'message': 'Details'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}

	/*
	*update Citation
	*/
	static async update(request, response){
		let status = 200;
		let body = {};

		try {
			let id = request.params.id;
			let citation= await citation.findById(id);
			await citation.update(request.body)
			body = {citation, 'message': 'Details'};
		}
		catch(error){
			status = 500;
			body = {'message': error.message}
		}
		return response.status(status).json(body);
	}
}
export default CitationController;